#include "Game.h"

/*
	Input: vector of players, questions number and the database
	C'tor
*/
Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	int _id = _db.insertNewGame(); //initiate the game id
	if (_id == -1)
	{
		throw exception();
	}
	
	//Initiate the rest of the details
	_currQuestionIndex = 0;
	_questions = _db.initQuestions(questionsNo);
	_players = players;
	_questionsNo = questionsNo;

	for (size_t i = 0; i < _players.size(); i++)//Go through all the players
	{
		//Initiate the user game to this game
		_players[i]->setGame(this);
		_results[_players[i]->getUsername()] = 0;
	}
}

/*
	Input: none
	D'tor - clears all the vectors
*/
Game::~Game()
{
	_questions.clear();
	_players.clear();
}

/*
	Input: none
	Output: the game id
*/
int Game::getId()
{
	return _id;
}

/*
	The function calls the a function to send the first question
*/
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

/*
	Input: none
	Output: none
	The funtion sends a question to all the users who are in the same game
*/
void Game::sendQuestionToAllUsers()
{
	//Get all the information and create the message
	string msg = "118";
	_currentTurnAnswers = 0;
	Question* question = _questions[_currQuestionIndex];
	string q = question->getQuestion();
	string answer1 = question->getAnswers()[0];
	string answer2 = question->getAnswers()[1];
	string answer3 = question->getAnswers()[2];
	string answer4 = question->getAnswers()[3];
	string qLen = padding(to_string(q.length()),3);
	string a1Len = padding(to_string(answer1.length()),3);
	string a2Len = padding(to_string(answer2.length()),3);
	string a3Len = padding(to_string(answer3.length()),3);
	string a4Len = padding(to_string(answer4.length()),3);

	msg += qLen + q + a1Len + answer1 + a2Len + answer2 + a3Len + answer3 + a4Len + answer4;

	for (size_t i = 0; i < _players.size(); i++) //Go through all the players
	{
		try 
		{
			_players[i]->send(msg); //Send th message
		}
		catch (...) {
		}
	}
}

/*
	Input: the string, the length
	Output: the string after the padding
*/
string Game::padding(string str, int num)
{
	string newStr = str;

	while (newStr.length() != num) //If the string isn't in the wanted size
	{
		newStr = "0" + newStr; //Add zeros
	}

	return newStr;
}

/*
	Input: the current user
	Output: if succeeded - true, else - false
*/
bool Game::leaveGame(User* currUser)
{
	bool succeeded = false;

	for (size_t i = 0; i < _players.size() && succeeded == false; i++) //Go through all the players and as long as the leaving wasn't successful
	{
		if (currUser == _players[i]) //If the current user if the wanted user
		{
			string msg = "121" + to_string(_players.size()); //Start creating the message
			string username = "";

			for (size_t j = 0; j < _players.size(); j++) //Go through all the players
			{
				//Get all the players usernames and add them to the list
				username = _players[j]->getUsername();
				msg += padding(to_string(username.length()), 2) + username + padding(to_string(_results[username]), 2);
			}
			
			std::map<User*, bool>::iterator it = _playersAnswers.find(_players[i]); //Find the players answer

			if (it != _playersAnswers.end()) //If the answer was found - ignore his answer in his round
			{
				_currentTurnAnswers--; //Reduce the turn answers in this round
				_playersAnswers.erase(it); //Erase his answer
			}

			_players[i]->setRoom(nullptr); //reset the room to null (the user leaved the room)
			_players.erase(_players.begin() + i); //Erase the user from the list of users in the game
			succeeded = handleNextTurn();
		}
	}

	return succeeded;
}

/*
	Input: none
	Output: if succeeded
	The function handles the ne
*/
bool Game::handleNextTurn()
{
	bool succeeded = true;

	if (_players.size() == 0) //If there is no more players in the game
	{
		handleFinishGame(); //Finish it
		succeeded = false;
	}
	else //If there are more players
	{
		if (_currentTurnAnswers == _players.size()) //The turn answers is the amount of players who stayed in the game after the user left
		{
			if (_questionsNo == _currQuestionIndex+1) //There is no more questions
			{
				handleFinishGame(); //Finish
				succeeded = false;
			}
			else //There is more questions
			{
				_playersAnswers.clear(); //Clear the answers vector
				_currQuestionIndex++; //Next question
				sendQuestionToAllUsers(); //Send the next question
			}
		}
	}

	return succeeded;
}

/*
	Input: none
	Output: none
	The function handles the finishing of the game
*/
void Game::handleFinishGame()
{
	size_t i = 0;
	_db.updateGameStatus(_id); //Update the game status to a finished game
	string msg = "121" + to_string(_players.size()); 
	string username = "";

	for (; i < _players.size(); i++)
	{
		//Add the players usernames to the message
		username = _players[i]->getUsername();
		msg += padding(to_string(username.length()), 2) + username + padding(to_string(_results[username]), 2);
	}

	for (i = 0; i < _players.size(); i++)
	{
		try
		{
			_players[i]->send(msg); //Send the message to everyone who is in the game
			
			//Initiate the game and room to null
			_players[i]->setGame(nullptr);
			_players[i]->setRoom(nullptr);
			
			//Update the users information
			_db.updateUsersInfo("score", _results[_players[i]->getUsername()], _players[i]->getUsername());
			_db.updateUsersInfo("gamesNum", 1, _players[i]->getUsername());
			_db.updateUsersInfo("avgTime", (_players[i]->getSumTime()) / ((_players[i]->getCorrectAnswersNum()) + _players[i]->getWrongAnswersNum()), _players[i]->getUsername());
		}
		catch (...) 
		{
		}
	}
}

/*
	Input: the user, his answer and how long it took to answer
*/
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	_currentTurnAnswers++;
	bool isCorrect = false;
	string msg = "1200";
	string answer = "";

	Question* question = _questions[_currQuestionIndex]; //Create the question object

	if (answerNo-1 == question->getCorrectAnswerIndex()) //If the answer is correct
	{
		answer = question->getAnswers()[answerNo - 1]; //get the answer
		isCorrect = true;
		msg = "1201";
		_results[user->getUsername()]++; //Add 1 point to score
		user->increaseCorrectAnswersNum(); //Add 1 correct answer
		_db.updateUsersInfo("correctAnswersNum", 1, user->getUsername()); //update the info
	}
	else if(answerNo != 5) //If the time ran out
	{
		answer = question->getAnswers()[answerNo - 1]; //get the answer
		user->increaseWrongAnswersNum(); //add 1 wrong answer
		_db.updateUsersInfo("wrongAnswersNum", 1, user->getUsername()); //update info
	}
	else //if the user answered wrong
	{
		user->increaseWrongAnswersNum(); //add 1 wrong answer
		_db.updateUsersInfo("wrongAnswersNum", 1, user->getUsername()); //update the info
	}

	user->setSumTime(time); //update time
	_db.addAnswerToPlayer(_id, user->getUsername(), _currQuestionIndex + 1, answer, isCorrect, time); //Add the answer to data base
	_playersAnswers[user] = true; 
	user->send(msg);
	return handleNextTurn();
}

/*
	Input: none
	Output: the players in game size
*/
int Game::getPlayersNum()
{
	return _players.size();
}

