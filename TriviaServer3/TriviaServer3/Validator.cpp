#include "Validator.h"

/*
	Input: password
	Output: if valid
	The function checks the password by the right conditions according to the protocol
*/
bool Validator::isPasswordValid(string password)
{
	bool len = password.length() >= 4;
	bool noSpaces = password.find(" ") == string::npos;
	bool oneDigit = (password.find_first_of("0123456789") != std::string::npos);
	bool oneUpperCase = (password.find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ") != std::string::npos);
	bool oneLowerCase = (password.find_first_of("abcdefghijklmnopqrstuvwxyz") != std::string::npos);

	return len && noSpaces && oneDigit && oneUpperCase && oneLowerCase;
}

/*
	Input: username
	Output: if valid
	The function checks the username by the right conditions according to the protocol
*/
bool Validator::isUserNameValid(string username)
{
	bool notEmpty = username.length() > 0;
	bool noSpaces = username.find(" ") == string::npos;
	bool firstIsLetter = (((username.substr(0, 1)).find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ") != std::string::npos) || ((username.substr(0, 1)).find_first_of("abcdefghijklmnopqrstuvwxyz") != std::string::npos));

	return notEmpty && noSpaces && firstIsLetter;
}
