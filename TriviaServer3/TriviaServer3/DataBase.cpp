#include "DataBase.h"

vector<Question*> questions;
vector<vector<string>> results;

/*
	Input: none
	C'tor - The function connect the programm to the right database
*/
DataBase::DataBase()
{
	char *zErrMsg = 0;
	int rc = sqlite3_open("KerenNoa.db", &_db);

	if (rc)
	{
		sqlite3_close(_db);
		throw exception("Can't open database");
	}

	//Get the questions
	string command = "select * from t_questions";
	rc = sqlite3_exec(_db, command.c_str(), callbackQuestions, 0, &zErrMsg);
}

/*
	Input: none
	D'tor - Closes the data base and clear the questions vector
*/
DataBase::~DataBase()
{
	sqlite3_close(_db);
	questions.clear();
}

/*
	Input: none
	Output: The questions vector size
*/
int DataBase::getQuestionsNumber()
{
	return questions.size();
}

/*
	Input: username
	Output: If the user exists - true, else - false
	The function checks if the specific user exists
*/
bool DataBase::isUserExists(string username)
{
	char *zErrMsg = 0;
	string command = "select * from t_users where username = '" + username + "'"; //Get the user from the database
	int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
	bool ans = (results.size() == 0) ? false : true; //Check if the results has the username
	clearTable();

	return ans;
}

/*
	Input: username, password and email
	Output: If succeeded to add a new user
	The function adds a new user to the table
*/
bool DataBase::addNewUser(string username, string password, string email)
{
	bool ans = true;

	try
	{
		char *zErrMsg = 0;
		string command = "insert into t_users values('" + username + "','" + password + "','" + email + "')"; //Insert new user in database
		int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
		clearTable();
	}
	catch (...) //If there is any error
	{
		ans = false;
	}

	return ans;
}

/*
	Input: username and password
	Output: if the password matches the username (succeesesful signin) - true, else - false
	The function checks if the username and password of the user are matching
*/
bool DataBase::isUserAndPassMatch(string username, string password)
{
	char *zErrMsg = 0;
	string command = "select * from t_users where username = '" + username + "' and password = '" + password + "'"; //get the user from the database
	int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
	bool ans = (results.size() == 1) ? true : false; //Check if match
	clearTable();

	return ans;
}

/*
	Input: the questions number
	Output: vector of the questions
	The function inits all the question with the questions from the data base
*/
vector<Question*> DataBase::initQuestions(int questionNo)
{
	vector<Question*> v;
	vector<int> numbers;
	int random = 0;
	
	for (int i = 0; i < questionNo; i++) //Go through all the questions
	{
		do
		{
			random = rand() % (questions.size()); //randomize the questions

		} 
		while (find(numbers.begin(), numbers.end(), random) != numbers.end()); //As long as this random number does not exist (to avoid the questions to return themselves)

		//Add the question to the vector
		numbers.push_back(random);
		v.push_back(questions[random]);
	}

	return v;
}

/*
	Input: none
	Output: how many games there is
	The function adds a new game to the table
*/
int DataBase::insertNewGame()
{
	char *zErrMsg = 0;
	string command = "insert into t_games(status, start_time) values(0, 'now')"; //insert new game
	int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
	clearTable();

	command = "select game_id from t_games"; //get all the games
	rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
	clearTable();
	return results.size();
}

/*
	Input: game id
	Output: if succeeded - true, else - false
	The function changes the specific game status to a specific details
*/
bool DataBase::updateGameStatus(int gameId)
{
	bool ans = true;

	try
	{
		char *zErrMsg = 0;
		string command = "update t_games set status=1,end_time='now' where game_id=" + to_string(gameId); //update the status of the game
		int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
		clearTable();
	}
	catch (...) //If there is any error
	{
		ans = false;
	}

	return ans;
}

/*
	Input: all the needed details for the answers table in database (game id, username, question id, the answer, if the answer is correct and how long it took to answer)
	Output: If succeeded - true, else - false
	The function adds to the table the answer of the player
*/
bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	bool ans = true;

	try
	{
		char *zErrMsg = 0;
		string command = "insert into t_players_answers values(" + to_string(gameId) + ",'" + username + "'," + to_string(questionId) + ",'" + answer + "'," + to_string(isCorrect) + "," + to_string(answerTime) + ")"; //Add the answer of the player
		int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
		clearTable();
	}
	catch (...) // If there is any error
	{
		ans = false;
	}
	
	return ans;
}

/*
	The function inserts all the wanted data from the database to the results vector
*/
int DataBase::callbackCount(void *NotUsed, int argc, char **argv, char **azColName)
{
	vector<string> v;

	for (int i = 0; i < argc; i++)
	{
		v.push_back(argv[i]);
	}
	results.push_back(v);

	return 0;
}

/*
	The function inserts the wanted questions from the database to the questions vector
	only in the c'tor (once)
*/
int DataBase::callbackQuestions(void *NotUsed, int argc, char **argv, char **azColName)
{
	Question* question = new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]);
	questions.push_back(question);
	return 0;
}

/*
	Input: none
	Output: none
	The function clears all the database data
*/
void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it) //clear all the data from the results vector
	{
		it->clear();
	}

	results.clear();
}

/*
	Input: username
	Output: none
	The function add a new username to the scores table
*/
void DataBase::addToScoreTable(string username)
{
	char *zErrMsg = 0;
	string command = "select * from users_info where username = '" + username + "'";
	int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
	bool ans = (results.size() == 1) ? true : false; //check if the user is in the score table
	clearTable();

	if (ans == false) //if the user is not in the score table
	{
		command = "insert into users_info values('" + username + "', 0, 0, 0, 0, 0)"; //add it
		rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
		clearTable();
	}
}

/*
	Input: the col name, the wanted value and the username
	Output: none
	The function updates the users info by the col 
*/
void DataBase::updateUsersInfo(string col, double value, string username)
{
	char *zErrMsg = 0;
	string command = "";
	int rc = 0;

	if (col.compare("avgTime") == 0) //If the updated info is the average answering time
	{
		command = "update users_info set " + col + "=" + to_string(value) + " where username='" + username + "'";
		rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
		clearTable();
	}
	else 
	{
		command = "update users_info set " + col + "=" + to_string(value) + "+" + col + " where username='" + username + "'";
		rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);
		clearTable();
	}
}

/*
	Input: none
	Output: vector of the best scores
	The function gets all the best scores
*/
vector<string> DataBase::getBestScores()
{
	char *zErrMsg = 0;
	string command = "select * from users_info order by score desc"; //get the 3 best scores (of users)
	int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);

	vector<string> bestScores;
	string username = "", correctAnswersNum = "", usernameLen = "";

	if (results.size() == 0) //If no one has scores yet
	{
		bestScores.push_back("000000");
	}
	else if (results.size() == 1) //If only one user has score
	{
		//Get the user details
		username = results[0][0]; 
		usernameLen = Game::padding(to_string(username.length()), 2);
		correctAnswersNum = Game::padding(results[0][3], 6);

		bestScores.push_back(usernameLen + username + correctAnswersNum + "000000000000"); //Insert the 1st user's details and zeros (because only one user has socres)
	}
	else if (results.size() == 2) //If two users has scores
	{
		//Get the users details
		username = results[0][0];
		usernameLen = Game::padding(to_string(username.length()), 2);
		correctAnswersNum = Game::padding(results[0][3], 6);

		bestScores.push_back(usernameLen + username + correctAnswersNum);

		username = results[1][0];
		usernameLen = Game::padding(to_string(username.length()), 2);
		correctAnswersNum = Game::padding(results[1][3], 6);

		bestScores.push_back(usernameLen + username + correctAnswersNum + "000000"); //Insert the details to the vector
	}
	else if (results.size() == 3) //If all three users has scores
	{
		//Get the users details and insert them to the vector
		username = results[0][0];
		usernameLen = Game::padding(to_string(username.length()), 2);
		correctAnswersNum = Game::padding(results[0][3], 6);

		bestScores.push_back(usernameLen + username + correctAnswersNum);

		username = results[1][0];
		usernameLen = Game::padding(to_string(username.length()), 2);
		correctAnswersNum = Game::padding(results[1][3], 6);

		bestScores.push_back(usernameLen + username + correctAnswersNum);

		username = results[2][0];
		usernameLen = Game::padding(to_string(username.length()), 2);
		correctAnswersNum = Game::padding(results[2][3], 6);

		bestScores.push_back(usernameLen + username + correctAnswersNum);
	}

	clearTable();

	return bestScores;
}

/*
	Input: username
	Output: vector of the user personal status
*/
vector<string> DataBase::getPersonalStatus(string s)
{
	char *zErrMsg = 0;
	string command = "select * from users_info where username='" + s + "'"; //Get the status
	int rc = sqlite3_exec(_db, command.c_str(), callbackCount, 0, &zErrMsg);

	string message = "";
	vector<string> personalStatus;

	if (results[0][2] == "0") //If there is no information
	{
		message = "00000000000000000000";
	}
	else
	{
		//Get all the information and connect it to message
		message = Game::padding(results[0][2], 4);
		message += Game::padding(results[0][3], 6);
		message += Game::padding(results[0][4], 6);

		string avgTime = results[0][5];
		string newAvgTime = "";

		for (size_t i = 0; i < avgTime.length(); i++)
		{
			if (avgTime[i] == '0' || avgTime[i] == '1' || avgTime[i] == '2' || avgTime[i] == '3' || avgTime[i] == '4' || avgTime[i] == '5' || avgTime[i] == '6' || avgTime[i] == '7' || avgTime[i] == '8' || avgTime[i] == '9')
			{
				newAvgTime += avgTime[i];
			}
		}

		message += Game::padding(newAvgTime, 4);
	}

	personalStatus.push_back(message); //Add the message to the vector
	clearTable();

	return personalStatus;
}