#include "Question.h"

/*
	Input: The question id, the question, the correct answer and the other 3 answers
*/
Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	_id = id;
	_question = question;
	_correctAnswerIndex = rand() % (3); //Randomize the correct answer index

	switch (_correctAnswerIndex) //Insert the questions according to which question is right
	{
	case 0:
		_answers[1] = answer2;
		_answers[2] = answer3;
		_answers[3] = answer4;
		break;
	case 1:
		_answers[0] = answer2;
		_answers[2] = answer3;
		_answers[3] = answer4;
		break;
	case 2:
		_answers[0] = answer2;
		_answers[1] = answer3;
		_answers[3] = answer4;
		break;
	case 3:
		_answers[0] = answer2;
		_answers[2] = answer3;
		_answers[1] = answer4;
		break;
	}

	_answers[_correctAnswerIndex] = correctAnswer; //Add the correct answer to the vector with the id
}

/*
	Input: none
	Output: the question
	The function returns the question itself
*/
string Question::getQuestion()
{
	return _question;
}

/*
	Input: none
	Output: a string array
	The function returns all the possible answers
*/
string* Question::getAnswers()
{
	return _answers;
}

/*
	Input: none
	Output: the correct answer index
	The function returns the index of the correct answer to the question
*/
int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

/*
	Input: none
	Output: id
	The function returns the id of the question
*/
int Question::getId()
{
	return _id;
}
