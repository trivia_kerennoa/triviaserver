#include "User.h"

/*
Input: username and client socket
The function is a c'tor
*/
User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currRoom = nullptr;
	_currGame = nullptr;
	_correctAnswersNum = 0;
	_wrongAnswersNum = 0;
	_sumTime = 0;
}

/*
Input: none
The function is a d'tor
*/
User::~User() { }

/*
Input: the message
Output: none
The function calls the function from helper in order to send the message
*/
void User::send(string msg)
{
	Helper::sendData(_sock, msg);
}

/*
Input: none
Output: username
The function returns the user username
*/
string User::getUsername()
{
	return _username;
}

/*
Input: none
Output: socket
The function returns the users socket
*/
SOCKET User::getSocket()
{
	return _sock;
}

/*
Input: room id, room name, maximum users in a room, number of questions, the time for each question
Output: if the function succeeded
The function creates room, and returns true if succeeded or false if didn'ts
*/
bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	bool ans = false;

	if (_currRoom != nullptr) //If the current room is already exists
	{
		send("1141"); //send faild message
	}
	else // If the room doesn't exist
	{
		//Create the room object
		_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		send("1140"); //Send succeeded message
		ans = true;
	}

	return ans;
}

/*
Input: none
Output: room object
The function returns a pointer to the users current room
*/
Room* User::getRoom()
{
	return _currRoom;
}

/*
Input: pointer to the new room
Output: if succeeded
The function calls joinRoom in the class Room and checks whether the user was able to enter the room or not
*/
bool User::joinRoom(Room* newRoom)
{
	bool succeeded = false;

	if (_currRoom == nullptr)	//if the user is not in another room
	{
		bool ans = newRoom->joinRoom(this);

		if (ans)	//if the user was able to enter the room
		{
			_currRoom = newRoom;
			succeeded = true;
		}
	}

	return succeeded;
}

/*
Input: none
Output: none
The function get the user out of the room by his request
*/
void User::leaveRoom()
{
	if (_currRoom != nullptr) //If the user is in a room
	{
		//Leave the room and initiate the room field into nullptr
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

/*
Input: none
Output: pointer to a game object
The function returns a pointer to the object game the user is currently in
*/
Game* User::getGame()
{
	return _currGame;
}

/*
Input: pointer to a game object
Output: none
The function sets the users current room into the room from the parameter
*/
void User::setGame(Game* gm)
{
	_currGame = gm;
}

/*
Input:	None
Output:	the room's id
The function closes the room
*/
int User::closeRoom()
{
	User* user = this;
	int answer = -1;
	if (_currRoom != nullptr)	//if the user is in any room
	{
		answer = _currRoom->getId();	//get the room's id
		_currRoom->closeRoom(user);		//close the room
		_currRoom = nullptr;	//so the user is no longer in the room
	}

	return answer;
}

/*
input:	None
Output:	None
Function clears the room
*/
void User::clearRoom()
{
	_currRoom = nullptr;
}

/*
input:	None
Output:	None
Function clears the game
*/
void User::clearGame()
{
	_currGame = nullptr;
}

/*
Input:	None
Output:	if succeeded
Function calls the function leaveGame that in the class Game and checks whether the user was able to leave the game or not
*/
bool User::leaveGame()
{
	bool succeeded = false;

	if (this != nullptr)	//if the user exists
	{
		succeeded = _currGame->leaveGame(this);
		//leaveGame in Game returns true if the game is still exists
		if (succeeded == false)	//if the user was able to leave the game or not
		{
			_currGame = nullptr;
		}
	}

	return succeeded;
}

/*
The function changes the value of the current room to the desired value
*/
void User::setRoom(Room* room)
{
	_currRoom = room;
}

/*
The function returns the correctAnswersNum
*/
int User::getCorrectAnswersNum()
{
	return _correctAnswersNum;
}

/*
The function returns the wrongAnswersNum
*/
int User::getWrongAnswersNum()
{
	return _wrongAnswersNum;
}

/*
The function returns the sumTime
*/
int User::getSumTime()
{
	return _sumTime;
}

/*
The function increase the correctAnswersNum by 1
*/
void User::increaseCorrectAnswersNum()
{
	_correctAnswersNum++;
}

/*
The function increase the wrongAnswersNum by 1
*/
void User::increaseWrongAnswersNum()
{
	_wrongAnswersNum++;
}

/*
The function increase the sumTime by the desired value
*/
void User::setSumTime(int sumTime)
{
	_sumTime += sumTime;
}
