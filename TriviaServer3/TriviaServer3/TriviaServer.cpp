#include "TriviaServer.h"
int TriviaServer::_roomIdSequence = 0;
std::condition_variable cv;

/*
Input: none
The function is a c'tor to the class
*/
TriviaServer::TriviaServer()
{
	_db = new DataBase();

	//Connect the socket and start a connection with the client
	WSADATA wsa_data = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw std::exception("WSAStartup Failed");

	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_socket == INVALID_SOCKET) // Check if the socket connection succeeded, if not - throw an exception
	{
		throw std::exception("invalid socket");
	}

	server();
}

/*
Input: none
The function is a d'tor that close the sockets and clean the maps
*/
TriviaServer::~TriviaServer()
{
	closesocket(_socket);
	_roomList.clear();
	_connectedUsers.clear();
}

/*
Input:none
Output: none
The function starts to listen to get messages from clients through the socket
*/
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT); // The port that the server will listen to
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY; //Any ip

									 //Connect between the socket and the port, ip, etc..
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	//Start to listen to requestes of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
}

/*
Input: none
Output: none
The function gets all the clients in an un-ending loop
*/
void TriviaServer::server()
{
	bindAndListen();
	std::thread t([=] { handleRecievedMessages(); });
	t.detach();

	while (true)
	{
		accept();
	}
}

/*
Input: none
Output: none
The fucntion gets a clients that connected to the server and creates a seperate thread for him
*/
void TriviaServer::accept()
{
	SOCKET clientSocket = ::accept(_socket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET) // I fthe client socket didn't succeeded - throw an exception
	{
		throw std::exception(__FUNCTION__);
	}

	std::thread t([=] { clientHandler(clientSocket); });
	t.detach();
}

/*
Input: client socket
Output: none
The function gets the message code from the client, creates the message and add it to the queue of messages from the client
*/
void TriviaServer::clientHandler(SOCKET clientSocket)
{
	int msgTypeCode = -1;
	std::string msg = "";

	try
	{
		while (msgTypeCode != 0 || msgTypeCode != 299) //As long there is no message to finish the connection
		{
			//Build the message and add it to the queue
			msg = Helper::getStringPartFromSocket(clientSocket, 1024);
			msgTypeCode = atoi(msg.substr(0, 3).c_str());
			RecievedMessage* rm = buildRecievedMessage(clientSocket, msgTypeCode, msg);
			addRecievedMessage(rm);
		}

		//Add a finish connection message
		addRecievedMessage(buildRecievedMessage(clientSocket, msgTypeCode, "299"));
	}
	catch (...)
	{
		//If there was a problem, ass a finish connection message
		addRecievedMessage(buildRecievedMessage(clientSocket, 299, "299"));
	}
}

/*
Input: client socket, the message code and the message
Output: a pointer to a recieved message
The function builds a message according to the message code
*/
RecievedMessage* TriviaServer::buildRecievedMessage(SOCKET socket, int msgCode, std::string msg)
{
	std::vector<std::string> values;
	std::string msgValues = msg.substr(3, msg.length() - 3);

	//switch case can't create a new string
	std::string username = "", password = "", email = "", roomId = "", roomName = "", playersNumber = "", questionsNumber = "", questionTimeInSec = "", answerNumber = "", timeInSeconds = "";
	int usernameLen = 0, passwordLen = 0, roomNameLen = 0, emailLen = 0;

	cout << "////////////////////////////////////////////////////\nMessage recieved: " << msg << endl;
	cout << "Message code: " << msgCode << endl;

	//Do function for each one?
	switch (msgCode)
	{
	case 200: // Signin request
		cout << "Type message code: Signin request" << endl;
		usernameLen = atoi(msgValues.substr(0, 2).c_str());
		passwordLen = atoi(msgValues.substr(usernameLen + 2, 2).c_str());
		username = msgValues.substr(2, usernameLen);
		password = msgValues.substr(usernameLen + 4, passwordLen);
		values.push_back(username);
		values.push_back(password);
		break;
	case 203: // Signup request
		usernameLen = atoi(msgValues.substr(0, 2).c_str());
		passwordLen = atoi(msgValues.substr(usernameLen + 2, 2).c_str());
		emailLen = atoi(msgValues.substr(usernameLen + passwordLen + 4, 2).c_str());
		username = msgValues.substr(2, usernameLen);
		password = msgValues.substr(usernameLen + 4, passwordLen);
		email = msgValues.substr(usernameLen + passwordLen + 6, emailLen);
		values.push_back(username);
		values.push_back(password);
		values.push_back(email);
		break;
	case 207: // room users request
		roomId = msgValues.substr(3, 4);
		values.push_back(roomId);
		break;
	case 209: // enter into an existing room request
		roomId = msgValues.substr(3, 4);
		values.push_back(roomId);
		break;
	case 213: // create new room request
		roomNameLen = atoi(msgValues.substr(0, 2).c_str());
		roomName = msgValues.substr(2, roomNameLen);
		playersNumber = msgValues.substr(roomNameLen + 2, 1);
		questionsNumber = msgValues.substr(roomNameLen + 3, 2);
		questionTimeInSec = msgValues.substr(roomNameLen + 5, 2);
		values.push_back(roomName);
		values.push_back(playersNumber);
		values.push_back(questionsNumber);
		values.push_back(questionTimeInSec);
		break;
	case 219: // clients answer
		answerNumber = msgValues.substr(0, 1);
		timeInSeconds = msgValues.substr(1, 2);
		values.push_back(answerNumber);
		values.push_back(timeInSeconds);
		break;
	}

	RecievedMessage* rm = new RecievedMessage(socket, msgCode, values); // Create a message object with the right parameters

	return rm;
}

/*
Input: message object
Output: none
The function add the message object to the messages queue
*/
void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	std::unique_lock<std::mutex> lock(_mtxRecievedMessages); //when we push message to the queue, no other thread can access the queue.
	_queRcvMessages.push(msg); //push the message to the queue

							   //after we finished using the queue, the access to the queue is open again
	lock.unlock();
	cv.notify_one();
}

/*
Input: message object
Oupput: none
The function handles the case when client wants to sign out
*/
void TriviaServer::handleSignout(RecievedMessage* msg)
{
	if (msg->getUser() != nullptr) //If the message has a user
	{
		_connectedUsers.erase(msg->getSock()); //erase the user from the map
	}
	bool closeRoom = handleCloseRoom(msg);
	bool leaveRoom = handleLeaveRoom(msg);
	handleLeaveGame(msg);

}


/*
Input: message object
Output: if the function succeeded or not
The function handles the case when a client leaves the room he is currently in
*/
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	bool succeeded = false;

	User* user = msg->getUser(); //Get the user who sent the message

	if (user != nullptr) //If there is a user to this message
	{
		Room* room = user->getRoom(); //Get the room the user is currently in 

		if (room != nullptr) //If the room exists
		{
			user->leaveRoom(); //Leave the room
			succeeded = true; //The funciton succeeded
							  //_roomIdSequence--;
		}
	}

	return succeeded;
}

/*
Input: message object
Output: if the function succeeded or not
The function handles the case of creating room
*/
bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	string roomName = "";
	bool succeeded = false, ans = false;
	int playersNumber = 0, questionsNumber = 0, questionTimeInSec = 0, roomId = 0;
	User* user = msg->getUser(); // Get the user who sent the message

	if (user != nullptr) //If the user exists
	{
		//Get the values from the message
		roomName = msg->getValues()[0];
		playersNumber = atoi(msg->getValues()[1].c_str());
		questionsNumber = atoi(msg->getValues()[2].c_str());
		questionTimeInSec = atoi(msg->getValues()[3].c_str());

		for (map<int, Room*>::iterator it = _roomList.begin(); it != _roomList.end(); ++it)
		{
			if (it->second->getName().compare(roomName) == 0)
			{
				ans = true;
			}
		}

		if (questionsNumber > _db->getQuestionsNumber() || ans == true)
		{
			user->send("1141");
		}
		else
		{
			bool ans = user->createRoom(_roomIdSequence, roomName, playersNumber, questionsNumber, questionTimeInSec); //create the room with the values from the message

			if (ans == true) //If the room has been created successfully
			{
				_roomList[_roomIdSequence] = user->getRoom();
				succeeded = true; //The function succeeded
				_roomIdSequence++; //Increase the number of room
			}
		}
	}

	return succeeded;
}

/*
Input: message object
Ouptut: if the function succeeded or not
The function handles the case of joining room
*/
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	bool succeeded = false;
	User* user = msg->getUser(); // Get the user who sent the message

	if (user != nullptr) // If the user exists
	{
		int id = atoi(msg->getValues()[0].c_str()); //get the room id
		Room* room = getRoomById(id); //Create room object by id

		if (room == nullptr) //If the room doesn't exist
		{
			user->send("1102"); //Sent failed message
		}
		else //If the room exist
		{
			bool ans = user->joinRoom(room); // let the user join the room
			succeeded = true; // The function suceeded
		}
	}

	return succeeded;
}

/*
Function gets socket and return the user
*/
User* TriviaServer::getUserBySocket(SOCKET clientSocket)
{
	map<SOCKET, User*>::iterator users = _connectedUsers.find(clientSocket);	//find the socket in the map of the connected users
	if (users == _connectedUsers.end())	//if the socket is not found
	{
		return nullptr;
	}
	//if the socket is found
	return users->second;
}

/*
Input: none
Output: none
The function handles the case of recieving message
*/
void TriviaServer::handleRecievedMessages()
{
	while (true) // un-ending loop
	{
		std::unique_lock<std::mutex> lock(_mtxRecievedMessages);
		cv.wait(lock);

		if (!_queRcvMessages.empty()) // As long as the messages queue isn't empty
		{

			RecievedMessage* rm = _queRcvMessages.front(); //Take the first message object
			_queRcvMessages.pop(); //relese from the queue in the front the message object 

								   //lock.unlock();

			try
			{
				rm->setUser(getUserBySocket(rm->getSock())); //Set the user of the message using the clients socket

															 //Take care of all the possible messages
				switch (rm->getMessageCode())
				{
				case 200:
					handleSignin(rm);
					break;
				case 201:
					handleSignout(rm);
					break;
				case 203:
					handleSignup(rm);
					break;
				case 205:
					handleGetRooms(rm);
					break;
				case 207:
					handleGetUsersInRoom(rm);
					break;
				case 209:
					handleJoinRoom(rm);
					break;
				case 211:
					handleLeaveRoom(rm);
					break;
				case 213:
					handleCreateRoom(rm);
					break;
				case 215:
					handleCloseRoom(rm);
					break;
				case 217:
					handleStartGame(rm);
					break;
				case 219:
					handlePlayerAnswer(rm);
					break;
				case 222:
					handleLeaveGame(rm);
					break;
				case 223:
					handleGetBestScores(rm);
					break;
				case 225:
					handleGetPersonalStatus(rm);
					break;
				case 299:
					safeDeleteUser(rm);
					break;
				default:
					safeDeleteUser(rm);
					break;
				}
			}
			catch (...)
			{
				safeDeleteUser(rm);
			}
		}
	}
}

/*
The function disconnects a user safely
*/
void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)	//If there is a user linked to the message
		{
			handleSignout(msg);
			closesocket(msg->getSock());	//close the socket
		}
	}
	catch (...)
	{
		cout << "didn't succeeded to delete the user" << endl;
	}
}

/*
Function gets username and return the user
*/
User* TriviaServer::getUserByName(string username)
{
	map<SOCKET, User*>::iterator it;
	User* user = nullptr;
	bool found = false;
	User* u = nullptr;

	for (it = _connectedUsers.begin(); it != _connectedUsers.end() && user == nullptr; ++it)	//all the users in the connected users map
	{
		u = it->second;
		user = (username.compare(u->getUsername()) == 0) ? u : nullptr;	//if the current user's name is equal to the desired username
	}
	return user;
}

/*
The function creates a new user and adds it to the list of connected users. The function sends the user a successful login message.
Output:	the new user
*/
User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	User* user = nullptr;
	string returnMsg = "102";	//the message code
	vector<string> info = msg->getValues();
	string username = info[0];
	string password = info[1];

	if (_db->isUserAndPassMatch(username, password) == false)	//if the user and the password is not matching
	{
		returnMsg += "1";
	}
	else if (getUserByName(username) != nullptr)	//if the user is already connected
	{
		returnMsg += "2";
	}
	else //if there is no problem
	{
		user = new User(username, msg->getSock());	//create new user
		_connectedUsers[msg->getSock()] = user;	//add it to the connected users' map
		returnMsg += "0";

		_db->addToScoreTable(username);	//add row in the scores' table
	}

	Helper::sendData(msg->getSock(), returnMsg);	//send the user a successful login message
	return user;
}

/*
The function adds the new user to the database and sends the user a successful registration message
Output:	if succeeded
*/
bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	bool answer = false;
	string returnMsg = "104";	//the message code
	vector<string> info = msg->getValues();
	string username = info[0];
	string password = info[1];
	string email = info[2];

	if (Validator::isPasswordValid(password) == false)	//if the password is illegal
	{
		returnMsg += "1";
	}
	else if (Validator::isUserNameValid(username) == false)	//if the username is illegal
	{
		returnMsg += "3";
	}
	else if (_db->isUserExists(username) == true)	//if the user is already exists
	{
		returnMsg += "2";
	}
	else	//if there is no problem
	{
		//add the new user to the data base
		if (_db->addNewUser(username, password, email) == true)
		{
			answer = true;
			returnMsg += "0";
		}
		else
		{
			returnMsg += "4";
		}
	}

	Helper::sendData(msg->getSock(), returnMsg);	//send the user a successful signup message
	return answer;
}

/*
The function closes the room
Output:	if succeeded
*/
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	bool answer = false;
	Room* room = (msg->getUser())->getRoom();
	if (room != nullptr)	//if the room exists
	{
		if ((msg->getUser())->closeRoom() != -1)	//if the room closed successfully
		{
			answer = true;
			_roomList.erase(room->getId());	//erase the room from the rooms' list
			_roomIdSequence--;
		}
	}
	return answer;
}

/*
Function gets list of the users in room
*/
void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	string returnMsg = "108";	//message code
	User* user = msg->getUser();
	int roomId = atoi((msg->getValues()[0]).c_str());
	Room* room = getRoomById(roomId);

	if (room == nullptr)	//if the room is not exists
	{
		returnMsg += "0";
	}
	else
	{
		string users = room->getUsersListMessage();
		returnMsg += users;	//build a message according to the protocol
	}

	user->send(returnMsg);	//send the message to the user

}

/*
Function gets the rooms' list
*/
void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	string numberOfRooms = to_string(_roomIdSequence);
	while (numberOfRooms.length() != 4)	//if the length of the numberOfRooms' length is not 4 bytes
	{
		numberOfRooms = "0" + numberOfRooms;
	}
	string returnMsg = "106" + numberOfRooms;

	for (map<int, Room*>::iterator it = _roomList.begin(); it != _roomList.end(); it++)
	{
		string id = to_string(it->first);
		while (id.length() != 4)	//if the length of the roomId's length is not 4 bytes
		{
			id = "0" + id;
		}

		string roomName = (it->second)->getName();
		string len = to_string(roomName.length());
		while (len.length() != 2)	//if the length of the roomName's length is not 4 bytes
		{
			len = "0" + len;
		}

		//build a message according to the protocol
		returnMsg += id + len + roomName;
	}
	(msg->getUser())->send(returnMsg);	//send the message to the user
}

/*
Function gets id and return the room
*/
Room* TriviaServer::getRoomById(int roomId)
{
	map<int, Room*>::iterator room = _roomList.find(roomId);	//find the room in the map
	if (room == _roomList.end())	//if the room is not found
	{
		return nullptr;
	}
	return room->second;
}

/*
Function starts the game
*/
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	try
	{
		User* user = msg->getUser();
		Room* room = user->getRoom();
		Game* game = new Game(room->getUsers(), room->getQuestionsNo(), *_db);	//create new game
		game->sendFirstQuestion();	//send the first question
		_roomList.erase(room->getId());	//delete the current room
		_roomIdSequence--;
	}
	catch (...)
	{
		(msg->getUser())->send("1180");
	}
}

/*
Function leaves the game
*/
void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	if (msg->getUser()->getGame() != nullptr)
	{
		Game* game = msg->getUser()->getGame();
		Room* room = msg->getUser()->getRoom();
		bool ans = msg->getUser()->leaveGame();

		//the function leaveGame in User return true if the game is still exists
		if (ans == false)
		{
			game->~Game();
		}
	}
}

/*
Function gets answer from player and take care of it
*/
void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	Game* game = msg->getUser()->getGame();
	Room* room = msg->getUser()->getRoom();

	if (game != nullptr)	//if the game is existst
	{
		//take care of the answer
		bool ans = game->handleAnswerFromUser(msg->getUser(), atoi(msg->getValues()[0].c_str()), atoi(msg->getValues()[1].c_str()));
		//handleAnswerFromUser return true if the game is still exists
		if (ans == false)	//if the game is ended
		{
			_roomIdSequence--;
			RecievedMessage* r = new RecievedMessage(msg->getSock(), 211);
			handleLeaveRoom(r);
		}
	}
}

/*
The function accepts a string and number of bytes
The function adds zeros at the beginning of the string until the length of the string is equal to the desired number of bytes.
Output: the new string
*/
string TriviaServer::padding(string str, int num)
{
	string newStr = str;
	while (newStr.length() != num)
	{
		newStr = "0" + newStr;
	}
	return newStr;
}

/*
Function gets best scores
*/
void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	vector<string> bestScores = _db->getBestScores();
	string message = "124";	//message code

	for (size_t i = 0; i < bestScores.size(); i++)
	{
		message += bestScores[i];	//build the message
	}

	(msg->getUser())->send(message);	//send the message to the user
}

/*
Function gets personal status
*/
void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	vector<string> personalStatus = _db->getPersonalStatus(msg->getUser()->getUsername());
	string message = "126";	//message code

	for (size_t i = 0; i < personalStatus.size(); i++)
	{
		message += personalStatus[i];	//build the message
	}

	(msg->getUser())->send(message);	//send the message to the user
}
