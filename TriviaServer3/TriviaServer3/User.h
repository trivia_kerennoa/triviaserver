#pragma once

#include "Helper.h"
#include "Room.h"
#include "Game.h"
class Game;
class Room;
using namespace std;

class User
{
private:
	std::string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
	int _correctAnswersNum;
	int _wrongAnswersNum;
	int _sumTime;

public:
	User(string username, SOCKET sock);
	~User();
	int getCorrectAnswersNum();
	int getWrongAnswersNum();
	int getSumTime();
	void increaseCorrectAnswersNum();
	void increaseWrongAnswersNum();
	void setSumTime(int sumTime);
	void send(string msg);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setRoom(Room* room);
	void setGame(Game* gm);
	void clearRoom();
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	void clearGame();
	bool leaveGame();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
};
