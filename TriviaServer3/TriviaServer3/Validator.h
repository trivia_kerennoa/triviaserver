#pragma once

#include <iostream>
#include <string>
using namespace std;

class Validator
{
public:
	static bool isPasswordValid(string password);
	static bool isUserNameValid(string username);
};
