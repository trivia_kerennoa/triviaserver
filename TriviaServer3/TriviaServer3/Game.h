#pragma once

#include <vector>
#include <map>
#include <string>
#include "Question.h"
#include "DataBase.h"
#include "User.h"

using namespace std;
class DataBase;
class User;

class Game
{
private:
	int _id;
	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	map<User*, bool> _playersAnswers;

	void sendQuestionToAllUsers();
	
public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	static string padding(string str, int num);
	bool leaveGame(User* currUser);
	int getId();
	int getPlayersNum();
};
