#include "Room.h"

/*
Input: room id, room name, the admin of the room, the maximum users of the room, the number of questions and the time for each question
The function is a c'tor
*/
Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionNo;
	_questionTime = questionTime;
	_users.push_back(admin);
}

Room::~Room()
{
	_users.clear();
}

/*
Input: none
Output: vector of user objects
The function returns the users vector
*/
vector<User*> Room::getUsers()
{
	return _users;
}

/*
Input: none
Output: the number of questions
The function returns the number of questions in this room
*/
int Room::getQuestionsNo()
{
	return _questionNo;
}

/*
Input: none
Output: id
The function returns the room id
*/
int Room::getId()
{
	return _id;
}

/*
Input: none
Output: name
The function returns the room name
*/
string Room::getName()
{
	return _name;
}

/*
	Input: none
	Output: the message
	The function gets the message of the users list
*/
string Room::getUsersListMessage()
{
	string msg = to_string(_users.size());
	
	for (size_t i = 0; i < _users.size(); i++) //Go through all the users
	{
		User* user = _users.at(i);
		string username = user->getUsername();
		string size = to_string((username).size());

		while (size.length() != 2) //Padding of the username
		{
			size = "0" + size;
		}

		msg += size;
		msg += user->getUsername();
	}

	return msg;
}

/*
	Input: the user
	Output: none
	The function handles the case of user leaving the room
*/
void Room::leaveRoom(User* excludeUser)
{
	size_t i = 0;
	bool isFound = false;

	for (i = 0; i < _users.size() && isFound == false; i++) //go through all the users
	{
		if (_users[i] == excludeUser) //If the user found in the vector
		{
			isFound = true;
		}
	}

	if (isFound == true) //If found
	{
		_users.erase(_users.begin() + i-1); //Delete the user from the vector =
		excludeUser->send("1120");

		string usersList = "108" + getUsersListMessage();
		sendMessage(usersList);
	}
}

/*
	Input: user
	Output: if succeeded
*/
bool Room::joinRoom(User* excludeUser)
{
	bool succeeded = false;

	if (_users.size() == _maxUsers) //if this is the maximum users in this room
	{
		excludeUser->send("1101");
	}
	else
	{
		string questionsNo = to_string(_questionNo), questionTime = to_string(_questionTime); 

		//Padding of the question
		while (questionsNo.length() != 2)
		{
			questionsNo = "0" + questionsNo;
		}

		while (questionTime.length() != 2)
		{
			questionTime = "0" + questionTime;
		}

		//Build the message
		string successMessage = "1100";
		successMessage += questionsNo;
		successMessage += questionTime;
		_users.push_back(excludeUser);
		excludeUser->send(successMessage); //Send the message with the room info

		string usersList = "108" + getUsersListMessage();
		sendMessage(usersList); //Send to all users that another user has joined

		succeeded = true;
	}

	return succeeded;
}

/*
	Input: message
	Output: none
	The function send the message to everyone
*/
void Room::sendMessage(string message)
{
	sendMessage(nullptr, message);
}

/*
	Input: user and message
	Output: none
	The function sends the message to everyone but the specific user
*/
void Room::sendMessage(User* excludeUser, string message)
{
	try 
	{
		for (size_t i = 0; i < _users.size(); i++)
		{
			if (_users[i] != excludeUser) //If it is the user, dont send
			{
				_users[i]->send(message);
			}
		}
	}
	catch (...) {}
}

/*
	Input: user
	Output: room id if succeeded, -1 if failed
*/
int Room::closeRoom(User* user)
{
	string closeMsg = "116";
	int answer = -1;

	if (_admin->getUsername().compare(user->getUsername()) == 0) //If the admin is the user who closes the room
	{
		sendMessage(closeMsg); //Send everyone a message of closing room
		answer = _id;
		for (size_t i = 0; i < _users.size(); i++)
		{
			if (_admin != user) //clear the room of everyone but the admin
			{
				(_users.at(i))->clearRoom();
			}
		}
	}

	return answer;
}

/*
	Input: none
	Output: the admins object
*/
User* Room::getAdmin()
{
	return _admin;
}