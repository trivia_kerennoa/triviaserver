#pragma once

#include <vector>
#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include "Question.h"
#include "Game.h"
#include "User.h"
#include <unordered_map>
#include "sqlite3.h"

using namespace std;

class DataBase
{
private:
	sqlite3* _db;
	vector<vector<string>> _users;
	static void clearTable();
	static int callbackCount(void *NotUsed, int argc, char **argv, char **azColName);
	static int callbackQuestions(void *NotUsed, int argc, char **argv, char **azColName);

public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password); //keren
	vector<Question*> initQuestions(int questionNo);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string s);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int i, string s, int i1, string s1, bool b, int i2);
	void addToScoreTable(string username);
	void updateUsersInfo(string col, double value, string username);
	int getQuestionsNumber();
};
